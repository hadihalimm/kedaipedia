# kedaipedia

[![Test and Deploy][actions-badge]][commits-gh]
[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

# kedaipedia - Your Needs, Our Priority
## Tugas Kelompok 2


## Anggota kelompok
1. Alvito Athallah (`1906302586`)
2. Hadi Halim Kamil (`1906398755`)
3. Galuh Vita Respati (`1906399404`)
4. Marthin Firman Thomas Lubis (`1906399631`)

## [`Link Heroku`](https://kedaipedia.herokuapp.com/)

## Cerita Aplikasi
Dengan adanya pandemi Covid-19, masyarakat kini memiliki pola hidup yang berbeda dari tahun-tahun sebelumnya, salah satunya perubahan pola berbelanja.  

kedaipedia merupakan website yang dapat memudahkan masyarakat membeli barang-barang kebutuhannya secara online.   
 
Dengan fitur-fitur yang memudahkan pengguna berbelanja online, masyarakat tidak perlu khawatir akan kualitas dan kuantitas produk yang ada di kedaipedia.

## Fitur
- search item
- add/delete item
- komentar/review item
- daftar wishlist

## TODO:
- [ ] 
- [ ]
- [ ] 
- [ ] 
- [ ] 
- [ ] 
- [ ] 
- [ ] 


[actions-badge]: https://gitlab.com/hadihalimm/kedaipedia/workflows/Test%20and%20Deploy/badge.svg
[commits-gh]: https://gitlab.com/hadihalimm/kedaipedia/commits/master
[pipeline-badge]: https://gitlab.com/hadihalimm/kedaipedia/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/hadihalimm/kedaipedia/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/hadihalimm/kedaipedia/-/commits/master