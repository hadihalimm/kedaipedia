from django.db import models

# Create your models here.

class Item(models.Model):
    nama_item = models.CharField(max_length=50)
    tipe_item = [
        ('ELT', 'Elektronik'),
        ('FNB', 'Food And Beverages'),
        ('CLO', 'Pakaian'),
        ('HEA', 'Produk Kesehatan atau Kecantikan'),
        ('KIT', 'Peralatan Dapur'),
        ('OFF', 'Peralatan kantor'),
        ('OTH', 'Lainnya')
    ]
    harga_item = models.IntegerField()
    deskripsi_item = models.TextField()
