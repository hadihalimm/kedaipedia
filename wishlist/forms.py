from django import forms
from .models import Harapan

class HarapanForms(forms.ModelForm):
    namaproduk=forms.CharField(label='')
    namaproduk.widget.attrs.update({'class':'wishlist-form', 'required':'required'})
    hargaproduk= forms.CharField(label='')
    hargaproduk.widget.attrs.update({'class':'wishlist-form', 'required':'required'})

    class Meta:
        model = Harapan
        fields = ['namaproduk','hargaproduk']