from django.shortcuts import render
from .models import Harapan
from .forms import HarapanForms

# Create your views here.

def daftarharapan(request):
    data = Harapan.objects.all() 
    form = HarapanForms()
    if request.method == "POST":  
        form = HarapanForms(request.POST)  
        if form.is_valid():
            try:  
                form.save()   
            except:  
                pass  
    else:  
        form = HarapanForms()

    response = {
        'form': form,
        'data' : data,
        
    }  
    return render(request, 'wishlist/daftarharapan.html')